package main

import (
	"flag"
	"github.com/stretchr/gomniauth"
	"github.com/stretchr/gomniauth/providers/google"
	"github.com/stretchr/objx"
	"html/template"
	"log"
	"net/http"
	"path/filepath"
	"sync"
)

type templateHandler struct {
	once     sync.Once
	filename string
	tpl      *template.Template
}

func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.tpl = template.Must(template.ParseFiles(filepath.Join("templates", t.filename)))
	})
	data := map[string]interface{}{
		"Host": r.Host,
	}
	if authCookie, err := r.Cookie("auth"); err == nil {
		data["UserData"] = objx.MustFromBase64(authCookie.Value)
	}

	_ = t.tpl.Execute(w, data)
}

func main() {
	var addr = flag.String("addr", ":8080", "The addr of the application.")
	flag.Parse()

	//setup gomniauth
	gomniauth.SetSecurityKey("asd")
	gomniauth.WithProviders(
		google.New(
			"1091889840723-8jv4n27amslhmaddqq6n37b521f4k0n3.apps.googleusercontent.com",
			"q8qcGKJFsNHZxQlTBK7GGMeh",
			"http://localhost:3000/auth/callback/google",
		),
	)

	//http.Handle("/assets/", http.StripPrefix("assets", http.FileServer(http.Dir("/path/to/assets"))))

	// root
	http.Handle("/chat", MustAuth(&templateHandler{filename: "chat.html"}))
	http.Handle("/login", &templateHandler{filename: "login.html"})
	http.HandleFunc("/auth/", loginHandler)

	r := newRoom()
	//r.tracer = trace.New(os.Stdout)
	http.Handle("/room", r)
	go r.run()

	log.Println("Starting web server on", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe", err)
	}
}
